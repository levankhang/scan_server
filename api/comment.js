var routes = require('express').Router()
var { Comment } = require('../core/dbModel');

routes.get('/', function(req, res){
    Comment.find()
        .then(function(docs){
            console.log(docs);
            res.send(docs)
        });
});

routes.get('/clear', function (req, res) {
    Comment.deleteMany()
        .then(function () {
            res.end('OK')
        })
})

module.exports = routes;