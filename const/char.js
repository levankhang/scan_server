module.exports = {
    SLASH: '/',
    DOT: '.',
    HTML: 'html',
    API: 'api',
    QUEST: '?',
    EQ: '=',
    COMMENT: 'comment',
    HTTPS: 'https',
    COLON: ':'
}