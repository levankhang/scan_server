var mongoose = require('./dbMongo');
var Schema = mongoose.Schema;

var CommentSchema = new Schema({
    ArticleID: String,
    ArticleUrl: String,
    Avatar: String,
    Comment: String,
    CreationDate: Number,
    Dislike: Number,
    Like: Number,
    DisplayName: String,
    Email: String,
    ID: String,
    ParentID: String,
    Source: String
});

var models = {
    Comment: mongoose.model('Comment', CommentSchema)
}

module.exports = models;
