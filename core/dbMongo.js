var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/db_comments', { useNewUrlParser: true }, function(error){
    if(error){
        console.log(error)
    }else{
        console.log('Connected to DB')
    }
})

module.exports = mongoose;