var ZingScan = require('./scans/zing');
var VnExpressScan = require('./scans/vnexpress');
var q = require('q');
var {Comment} = require('./dbModel');

process.on('message', (data) => {
    console.log('Scan data: ', data)
    let zingScan = new ZingScan(data.scanPagesZing);
    let vnexpressScan = new VnExpressScan(data.scanPagesVnExpress);

    q.when()
        .then(function () {
            return Comment.deleteMany();
        })
        .then(function () {
            let taskZing = zingScan.scan()
                .then(function (cmts) {
                    // return Comment.insertMany(cmts);
                })
                .then(function (cmts) {
                    process.send({
                        type: 'SCAN_DONE',
                        source: 'zing',
                        data: cmts
                    })
                    return true;
                }).progress(function (progress) {
                    if(progress.cmts){
                        if(progress.cmts.length > 0){
                            Comment.insertMany(progress.cmts)
                                .then(function () {
                                    process.send(progress);
                                })
                        }
                    }else{
                        process.send(progress);
                    }
                });

            let taskVnExpress = vnexpressScan.scan()
                .then(function (cmts) {
                    return Comment.insertMany(cmts);
                })
                .then(function (cmts) {
                    process.send({
                        type: 'SCAN_DONE',
                        source: 'vnexpress',
                        data: cmts
                    })
                    return true;
                }).progress(function (progress) {
                    if(progress.cmts){
                        if(progress.cmts.length > 0){
                            Comment.insertMany(progress.cmts)
                                .then(function () {
                                    process.send(progress);
                                })
                        }
                    }else{
                        process.send(progress);
                    }
                });

            return q.all([taskZing, taskVnExpress])
        })
        .then(function (arguments) {
            console.log('All scan ok', arguments)
        })

})

process.on('SIGINT', function () {
    console.log('Exiting', process.pid);
    process.exit(0)
})
process.on('exit', (code) => {
    console.log(`About to exit with code: ${code}`);
});

