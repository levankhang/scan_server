var Http = require('../../libs/http');
var HostVnExpress = require('../../config/host').vnexpress;
var CharConst = require('../../const/char');
var q = require('q');
var Cheerio = require('cheerio');
var _ = require('../../libs/lodash');

class VnExpress{
    constructor(totalPages){
        this.total_pages = totalPages;
        this.category = 'phap-luat';
    }
    getListArticle(pageIndex, body){
        var tasks = [];
        var listArticlesUrl = [];
        var self = this;
        var parseArticleUrl = function(pageIndex,body){
            var result = [];
            if(!body){
                return result;
            }
            var $ = Cheerio.load(body);
            var articleTitles = $('.sidebar_1 article').find('.title_news');

            articleTitles.each(function(i, el){
                let url = $(this).children().attr('href');
                result.push(url);
            })
            return result;
        }

        for(let i = 1; i <= self.total_pages; ++i){
            let url = 'https://' + HostVnExpress.url + CharConst.SLASH + self.category;
            if(i != 1){
                url += `-p${i}`;
            }
            let task = Http.get(url)
            .then(function(body){
                return body;
            });

            tasks.push(task);
        }
        return q.all(tasks)
        .then(function(bodys){
            bodys.forEach(function(body, i){
                let urls = parseArticleUrl(i + 1, body);
                listArticlesUrl = listArticlesUrl.concat(urls);
            });
            return listArticlesUrl;
        })
        .catch(function(error){
            console.log("Error VnExpress: ", error)
        })
    }
    getComments(articleUrl){
        var result = [];
        var parseComments = function(body){
            if(!body){
                return []
            }
            let $ = Cheerio.load(body, {
                decodeEntities: false
            });
            let data = {};
            let stringInput = "";
            try {
                stringInput = $('#box_comment_vne').attr('data-component-input');
                data = JSON.parse(stringInput);
            }catch(e) {
                return result;
            }
            var post = {};
            post.objectid=data.article_id;
            post.objecttype=data.article_type;
            post.siteid=data.site_id;
            post.categoryid=data.category_id;
            post.offset=0;
            post.limit=Number.MAX_SAFE_INTEGER;
            post.sign=data.sign;
            let url = "https://usi-saas.vnexpress.net" + "/index/get?";

            Object.keys(post).forEach(function(key){
                url += (`${key}=${post[key]}&`)
            });
       
            return Http.get(url,post)
                .then(function(response){
                    if(!response){
                        return result;
                    }
                    var comments = response.data.items;
                    comments.forEach(function(cmt){
                        let replys = Object.assign({}, cmt.replys);
                    
                        if(replys.total){
                            let child_cmts= replys.items;
                            if(child_cmts){
                                child_cmts.forEach(function(child_cmt){
                                    child_cmt.ArticleUrl = articleUrl;
                                    result.push(child_cmt);
                                })
                            }
                        
                        }
                        cmt.ArticleUrl = articleUrl;
                        result.push(cmt);
                    });
                    // console.log("=== cmt", articleUrl, result)
                    return result;
                })
                .catch(function(error){
                    console.log('Get comments error: ', error);
                    return []
                })
        }
        return Http.get(articleUrl)
           .then(function(body){
                var cmts = parseComments(body);
                return cmts;
           })
           .catch(function(error){
                console.log('Get article error', error);
                return [];
           })
    }

    writeCsv(comments){
        const fastcsv = require('fast-csv');  
        const fs = require('fs');  
        const ws = fs.createWriteStream("out_vnexpress.csv", {encoding: 'utf8'});  
        fastcsv.write(comments, { headers: true }).pipe(ws);
        console.log('Scan VnExpress OK: ' + comments.length + ' scanned');
    }
    // Main function
    scan(){
        var self = this;
        var defer = q.defer();
        var doneArticleCount = 0;
        var doneCmtCount = 0;

        q.when()
        .then(function(){
            return self.getListArticle();
        })
        .then(function(listArticlesUrls){
            defer.notify({
                type: 'SCAN_INIT_INFO',
                source: 'vnexpress',
                totalArticles: listArticlesUrls.length
            });

            let tasks = listArticlesUrls.map(function(url){
                return self.getComments(url)
                .then(function(cmts){
                    doneArticleCount ++;
                    doneCmtCount += cmts.length;
                    defer.notify({
                        type: 'SCAN_PROGRESS',
                        doneArticleCount,
                        doneCmtCount,
                        source: 'vnexpress',
                        cmts: cmts.map(function(cmt){
                            if(!cmt){
                                return;
                            }
                            let comment = {};
                            comment.Comment = cmt.content;
                            comment.CreationDate = cmt.creation_time;
                            comment.Email = null;
                            comment.Like = cmt.userlike;
                            comment.DisplayName = cmt.full_name || '';
                            comment.ID = cmt.comment_id;
                            comment.ParentID = cmt.parent_id;
                            comment.Dislike = 0;
                            comment.ArticleID = cmt.article_id;
                            comment.Avatar = cmt.avatar || cmt.avatar_original;
                            comment.Source = 'VnExpress'
                            comment.ArticleUrl = cmt.ArticleUrl;
                            return comment;
                        })
                    })
                    return cmts;
                });
            })
            return q.all(tasks);
        })
        .then(function(){
            let allComments = _.flatten(arguments[0]);
            defer.resolve(allComments);
        })
        .catch(function(error){
            console.log('VnExpress Scan Error: ', error)
        })

        return defer.promise;
    }
}

module.exports = VnExpress;