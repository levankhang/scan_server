var Http = require('../../libs/http');
var HostZing = require('../../config/host').zing;
var CharConst = require('../../const/char');
var q = require('q');
var Cheerio = require('cheerio');
var _ = require('../../libs/lodash');

class Zing {
    constructor(total_pages) {
        this.category = 'the-gioi';
        this.page_text = 'trang';
        this.total_pages = total_pages;
    }

    getListArticle() {
        var tasks = [];
        var listArticlesId = [];
        var self = this;

        var parseIdArticle = function (pageIndex, body) {
            let $ = Cheerio.load(body);
            // console.log(body)

            let result = [];
            try {
                let selector = '.section-content>.article-list';
                if (pageIndex == 1) {
                    selector = '#news-latest ' + selector;
                }

                let articles = $(selector).find('article');

                articles.each(function () {
                    let article_id = $(this).attr('article-id');
                    let article_url = 'https://news.zing.vn' + $(this).find('.article-thumbnail').children('a').attr('href');
                    // console.log(article_id, article_url);
                    // let topic_ids = $(this).attr('topic-id').split(',');
                    // console.log(article_id, topic_ids)
                    result.push({id: article_id, url: article_url});
                });
                return result;
            } catch (e) {
                console.log('Parsing error: ', err);
                return []
            }

        }

        for (let i = 1; i <= this.total_pages; ++i) {
            let url = CharConst.HTTPS
                + CharConst.COLON
                + CharConst.SLASH
                + CharConst.SLASH
                + HostZing.url
                + CharConst.SLASH
                + this.category
                + CharConst.SLASH
                + this.page_text
                + i
                + CharConst.DOT
                + CharConst.HTML;

            let task = q.when()
                .then(function () {
                    return Http.get(url);
                })
                .then(function (body) {
                    return body;
                }).catch(function (error) {
                    console.log('Task get body fail: ', error)
                });

            tasks.push(task);
        }
        ;

        return q.all(tasks)
            .then(function (bodys) {
                bodys.forEach(function (body, i) {
                    if (!body) {
                        return;
                    }
                    let ids = parseIdArticle(i + 1, body);
                    listArticlesId = listArticlesId.concat(ids);
                });
                return listArticlesId;
            })
            .catch(function(err){
                console.log('Task get ids fail: ', err)
            });
    }

    getComments(articleId) {
        let url = 'https://api.news.zing.vn/api/comment.aspx?action=get&id=' + articleId.id;
        var results = [];
        return Http.get(url)
            .then(function (response) {
                if (!response) {
                    return results;
                }
                response.comments.forEach(function (cmt) {
                    cmt.ParentID = null;
                    cmt.ID = cmt.CreationDate;
                    cmt.ArticleID = articleId.id;
                    cmt.ArticleUrl = articleId.url;
                    cmt.Source = 'Zing'
                    if (cmt.ReplyCount != 0) {
                        let replies = Object.assign({}, cmt).Replies;
                        replies.forEach(function (child_cmt) {
                            child_cmt.ParentID = cmt.ID;
                            child_cmt.ID = child_cmt.CreationDate;
                            child_cmt.ArticleID = articleId.id;
                            child_cmt.ArticleUrl = articleId.url;
                            child_cmt.Source = 'Zing';
                            delete child_cmt.Replies;
                            results.push(child_cmt);
                        })
                    }
                    delete cmt.Replies;
                    results.push(cmt);
                })
                return results;
            });
    }

    writeCsv(comments) {
        const fastcsv = require('fast-csv');
        const fs = require('fs');
        const ws = fs.createWriteStream("out_zing.csv", {encoding: 'utf8'});
        fastcsv.write(comments, {headers: true}).pipe(ws);
        console.log('Scan Zing OK: ' + comments.length + ' scanned');
    }

    //  Main function
    scan() {
        var self = this;
        var defer = q.defer();
        var doneArticleCount = 0;
        var doneCmtCount = 0;

        q.when()
            .then(function () {
                return self.getListArticle();
            })
            .then(function (listArticlesId) {
                defer.notify({
                    type: 'SCAN_INIT_INFO',
                    source: 'zing',
                    totalArticles: listArticlesId.length
                });

                let tasks = listArticlesId.map(function (id) {
                    return self.getComments(id)
                        .then(function (cmts) {
                            doneArticleCount++;
                            doneCmtCount += cmts.length;
                            defer.notify({
                                type: 'SCAN_PROGRESS',
                                doneArticleCount,
                                doneCmtCount,
                                source: 'zing',
                                cmts
                            })
                            return cmts;
                        });
                })
                return q.all(tasks);
            })
            .then(function () {
                let allComments = [];
                arguments[0].forEach(function (cmts) {
                    allComments = allComments.concat(cmts);
                });

                defer.resolve(allComments);
            })
            .catch(function(err){
                console.log('Scan error: ', err)
            });

        return defer.promise;
    }
}

module.exports = Zing;