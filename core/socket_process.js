var BaseEvent = require('../const/socket_base_events');
var { fork } = require('child_process');
class SocketProcess {
    init(http) {
        var self = this;
        this.io = require('socket.io')(http);
        this.io.on('connection', function (socket) {
            console.log('A new user: ', socket.id)
            self.registerBaseEvent(socket);
        });
        this.process_map = {};
    }
    registerBaseEvent(socket) {
        var self = this;
        socket.on(BaseEvent.SCAN, function (data) {
            let forked = fork(__dirname + '/scan_process.js')

            forked.on('message', (data) => {
                socket.emit(BaseEvent.SCAN_DATA, data);
            })

            forked.send(data);
            self.process_map[socket.id] = forked.pid;
        });

        socket.on(BaseEvent.STOP, function () {
            if(self.process_map[socket.id]){
                let pid = self.process_map[socket.id];
                console.log('call stop because user')
                process.kill(pid, 'SIGINT');
                delete self.process_map[socket.id]
            }
        });

        socket.on('disconnect', function(){
            if(self.process_map[socket.id]){
                let pid = self.process_map[socket.id];
                console.log('call stop because socket disconnect')
                process.kill(pid, 'SIGINT');
                delete self.process_map[socket.id]
            }
        })
    }
}

module.exports = new SocketProcess();