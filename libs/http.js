var _ = require('./lodash');

class Http{
    get(url, params, options){
        return _.sendRequest('get', url, params, options)
    }

    post(url, params, options){
        return _.sendRequest('post', url, params, options)
    }
}

module.exports = new Http(); 