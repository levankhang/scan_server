var _ = require('lodash');
var axios = require('axios')

module.exports = _.mixin({
    sendRequest(method, path, params, options) {
        options = options || {}
        let config = {};
        let urlRequest = path;
        if (options.isFullPath) {
            urlRequest = path;
        }

        return new Promise(function (resolve, reject) {
            let maxRetry = 10;
            let retry = function () {
                axios[method](urlRequest, params, config)
                    .then(function (res) {
                        resolve(res.data)
                    }).catch(function () {
                        if(maxRetry <= 0){
                            reject(null);
                            return;
                        }
                        console.log('Error: ', urlRequest, ' , retry: ', maxRetry);
                        setTimeout(function(){
                            maxRetry --;
                            retry();
                        }, 1000);
                    });
            }

            retry();
        });
    }
})