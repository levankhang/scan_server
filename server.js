var app = require('express')();
var http = require('http').Server(app);
const cors = require('cors');
const bodyParser = require('body-parser');
const SocketProcess = require('./core/socket_process');
// config middleware

app.use(cors());
app.use(bodyParser.json());

// routes api
app.use('/comment', require('./api/comment'));

http.listen('3001', () => {
    SocketProcess.init(http);
    console.log('Server listening port 3001...');
});


process
    .on('unhandledRejection', (reason, p) => {
        console.error(reason, 'Unhandled Rejection at Promise', p);
    })
    .on('uncaughtException', err => {
        console.error(err, 'Uncaught Exception thrown');
        process.exit(1);
    });